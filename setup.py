import os
import subprocess

from setuptools import find_packages, setup
from setuptools.command.sdist import sdist


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        with open('VERSION', 'w') as fd:
            fd.write(version)
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result.replace('.dirty', '+dirty')
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


setup(
    name='gitea_redmine',
    version=get_version(),
    description='Micro serveur HTTP pour intercepter les webhooks Gitea et mettre à jour les tickets redmine correspondants avec les bons status / liens vers pull requests.',
    author='Agate Berriot',
    author_email='aberriot@entrouvert.com',
    cmdclass={
        'sdist': eo_sdist,
    },
    packages=find_packages(exclude=['tests']),
    py_modules=['gitea_redmine'],
    include_package_data=True,
    url='https://gitea.entrouvert.org/entrouvert/gitea-redmine',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    install_requires=['python-redmine', 'flask'],
    zip_safe=False,
)
