Un webservice qui traite les webhooks Gitea pour enricher les tickets correspondants sur Redmine.

## Fonctionnalités

- Ajouter un lien vers une pull request Gitea dans les tickets correspondants sur Redmine
- Changer le statut d'un ticket vers En cours quand la pull request est créée
- Changer le statut d'un ticket vers Résolu quand la pull request est mergée
- Assigner le ticket redmine à l'auteur de la pull request qui est ouverte
- Passer un ticket redmine à En Cours quand une pull request est en WIP
- Informer sur le ticket redmine quand une pull request est relue
- Passer un ticket redmine à validé quand un pull request est validée par une personne

## Environnement de développement

```bash
pip install flask python-redmine

# Variables d'environnement nécessaires
## avec cette valeur, l'URL de webhook à utiliser sera de la forme /incoming-webhook/abcdefgh
export INCOMING_WEBHOOK_SECRET=abcdefgh
## clé d'API redmine à récupérer sur https://dev.entrouvert.org/my/api_key
export REDMINE_API_KEY=xxx
export REDMINE_URL=https://dev.entrouvert.org/

# lancer le serveur en développement
flask --app gitea_redmine --debug run
```

## Configuration Redmine

Côté Redmine, créer un utilisateur gitea dédié et lui donner les droits administrateurs. Les droits administrateurs sont malheureusement requis pour pouvoir faire le mapping entre les utilisateurs gitea et les utilisateurs redmine (cf https://www.redmine.org/projects/redmine/wiki/Rest_Users).

Récurpérer la clé d'API de l'utilisateur qui servira pour le déploiement.

## Configuration Gitea

Côté Gitea, configurer [un webhook au niveau de l'organisation](https://gitea.entrouvert.org/org/entrouvert/settings/hooks)
pour éviter de le faire par projet. Cocher « Custom events » et les évenements « Pull request » et « Pull request reviewed».

## Déploiement

```
git clone gitea@gitea.entrouvert.org:entrouvert/gitea-redmine.git /home/gitea-redmine
cd /home/gitea-redmine
sudo apt install python3-venv python3-pip
python3 -m venv venv
./venv/bin/pip install gunicorn
./venv/bin/pip install -e .

### SystemD

```
[Unit]
Description=Gitea Redmine (Webhook forwarder)
After=syslog.target
After=network.target

[Service]
RestartSec=2s
Type=simple
User=www-data
Group=www-data
WorkingDirectory=/home/gitea-redmine/
ExecStart=/home/gitea-redmine/venv/bin/gunicorn --bind 127.0.0.1:5000 gitea_redmine:app
Restart=always
Environment=REDMINE_URL=https://dev.entrouvert.org/ INCOMING_WEBHOOK_SECRET=abcdefgh REDMINE_API_KEY=xxx

[Install]
WantedBy=emulti-user.target

## Tests

Lancer classiquement via `tox`.
